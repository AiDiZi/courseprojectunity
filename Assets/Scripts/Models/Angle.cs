﻿namespace Assets.Scripts.Models
{
    public class Angle
    {

        public int Deg { get; set; }

        public int Minute { get; set; }

        public int Second { get; set; }

        public Angle(int deg, int minute, int second)
        {
            Deg = deg;
            Minute = minute;
            Second = second;
        }

    }
}
