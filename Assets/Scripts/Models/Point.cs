﻿using UnityEngine;

namespace Assets.Scripts.Models
{
    public class Point
    {
        public double Teta { get; }

        public double Fi { get; }

        public double Radius { get; }

        public Vector3 PointIntersection { get; }

        public Point(double teta, double fi, double radius = 0, Vector3 pointIntersection = new Vector3())
        {
            Teta = teta;
            Fi = fi;
            Radius = radius;
            PointIntersection = pointIntersection;
        }

        public bool Equals(Point point)
        {
            if (Teta == point.Teta && Fi == point.Fi)
                return true;
            return false;
        }

    }
}
