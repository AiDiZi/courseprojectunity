﻿using Assets.Scripts.BusinessLogic;
using Assets.Scripts.Helpers.Writer;
using Assets.Scripts.Models.Enums;
using System.Collections.Generic;

namespace Assets.Scripts.Models
{
    public class FileContextDictionary
    {
        private static Dictionary<int, IFormatStr> fileDictionary = new Dictionary<int, IFormatStr>()
        {
            {(int)OutFormatFile.ExelFile,  new CSVFormat()},
            {(int)OutFormatFile.TextFile, new TxtFormat() }
        };

        public static Dictionary<int, IFormatStr> FileDictionaty
        {
            get
            {
                return fileDictionary;
            }
        }
    }
}
