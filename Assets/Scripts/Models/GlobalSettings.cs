﻿using Assets.Scripts.Models.Enums;
using System.Collections.Generic;

namespace Assets.Scripts.Models
{
    public static class GlobalSettings
    {
        public static int StepAngle { get; set; }

        public static int StepAngleMinute { get; set; }

        public static int StepAngleSecond {get;set;}

        public static OutFormatFile OutFormatFile { get; set; }

        public static string OutFile { get; set; }

        public static List<string> ModelsForScan { get; set; }

        public static List<string> MapsForScan { get; set; }

        public static void SetAllProperty(int stepAngle,int stepAngleMinute, int stepAngleSecond,OutFormatFile outFormatFile, List<string> modelsForScan, List<string> mapsForScan, string outFile)
        {
            StepAngle = stepAngle;
            StepAngleMinute = stepAngleMinute;
            StepAngleSecond = stepAngleSecond;
            OutFormatFile = outFormatFile;
            ModelsForScan = modelsForScan;
            MapsForScan = mapsForScan;
            OutFile = outFile;
        }
    }
}
