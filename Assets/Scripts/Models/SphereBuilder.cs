﻿namespace Assets.Scripts.Models
{
    public class SphereBuilder
    {

        private LinkedPointSphere GetStartPoint(double stepAngle)
        {
            var startPoint = new LinkedPointSphere(new Point(2 * stepAngle, 0));
            startPoint.LeftPoint = new LinkedPointSphere(new Point(2 * stepAngle, 360 - stepAngle));
            startPoint.RightPoint = new LinkedPointSphere(new Point(2 * stepAngle, stepAngle));
            startPoint.TopLeftPoint = new LinkedPointSphere(new Point(stepAngle, 360 - stepAngle));

            startPoint.TopPoint = new LinkedPointSphere(new Point(stepAngle, 0));
            startPoint.TopRightPoint = new LinkedPointSphere(new Point(stepAngle, stepAngle));
            startPoint.BottomPoint = new LinkedPointSphere(new Point(3 * stepAngle, 0));

            startPoint.BottomLeftPoint = new LinkedPointSphere(new Point(3 * stepAngle, 360 - stepAngle));
            startPoint.BottomRightPoint = new LinkedPointSphere(new Point(3 * stepAngle, stepAngle));

            return startPoint;
        }

        public LinkedPointSphere GetSphere( double stepAngle)
        {
            var startPoint = GetStartPoint( stepAngle);

            var linkedPointSphere = startPoint;

            for (double j = stepAngle; j < 360; j += stepAngle)
            {
                for (double i = stepAngle; i < 360; i += stepAngle)
                {
                    linkedPointSphere = GetCircleFi(stepAngle, new LinkedPointSphere(new Point(0, 0)), linkedPointSphere);
                }

                linkedPointSphere = FillOneCircleByFi(linkedPointSphere);

                linkedPointSphere = GetCircleTeta(stepAngle, new LinkedPointSphere(new Point(0, 0)), linkedPointSphere);
            }

            return linkedPointSphere;
        }
        private LinkedPointSphere FillOneCircleByFi(LinkedPointSphere linkedPointSphere)
        {
            var startPointFi = RoollbackToStartFi(linkedPointSphere, 0);

            startPointFi.LeftPoint = linkedPointSphere;
            startPointFi.TopLeftPoint = linkedPointSphere.TopPoint;
            startPointFi.BottomLeftPoint = linkedPointSphere.BottomPoint;

            linkedPointSphere.TopRightPoint = startPointFi.TopPoint;
            linkedPointSphere.RightPoint = startPointFi;
            linkedPointSphere.BottomRightPoint = startPointFi.BottomPoint;

            linkedPointSphere = linkedPointSphere.RightPoint;

            return linkedPointSphere;
        }

        private LinkedPointSphere LinkStartEndPoints(LinkedPointSphere topPoint, LinkedPointSphere bottomPoint)
        {
            bottomPoint.TopPoint = topPoint;
            bottomPoint.TopRightPoint = topPoint.RightPoint;
            bottomPoint.TopLeftPoint = topPoint.LeftPoint;

            topPoint.BottomLeftPoint = bottomPoint.LeftPoint;
            topPoint.BottomPoint = bottomPoint;
            topPoint.BottomRightPoint = bottomPoint.RightPoint;

            return topPoint;
        }

        private LinkedPointSphere RoollbackToStartFi(LinkedPointSphere linkedPointSphere, double angle)
        {
            if (angle == linkedPointSphere.Coordinat.Fi)
                return linkedPointSphere;
            else
                return RoollbackToStartFi(linkedPointSphere.LeftPoint, angle);
        }

        private LinkedPointSphere RoollbackToStartTeta(LinkedPointSphere linkedPointSphere, double angle)
        {
            if (angle == linkedPointSphere.Coordinat.Teta)
                return linkedPointSphere;
            else
                return RoollbackToStartTeta(linkedPointSphere.TopPoint, angle);
        }

        public LinkedPointSphere GetCircleTeta(double stepAngle, LinkedPointSphere linkedPointSphere, LinkedPointSphere lastLinkedSphere)
        {
            linkedPointSphere = lastLinkedSphere.BottomPoint;
            linkedPointSphere.TopPoint = lastLinkedSphere;
            linkedPointSphere.TopRightPoint = lastLinkedSphere.RightPoint;
            linkedPointSphere.TopLeftPoint = lastLinkedSphere.LeftPoint;

            linkedPointSphere.LeftPoint = lastLinkedSphere.BottomLeftPoint;
            linkedPointSphere.RightPoint = lastLinkedSphere.BottomRightPoint;

            var bottomRightPointCoordinat = lastLinkedSphere.BottomRightPoint.Coordinat;
            var bottomPointCoorinat = lastLinkedSphere.BottomPoint.Coordinat;
            var bottomLeftPointCoordinat = lastLinkedSphere.BottomRightPoint.Coordinat;

            linkedPointSphere.BottomRightPoint = new LinkedPointSphere(new Point(bottomRightPointCoordinat.Teta + stepAngle, bottomRightPointCoordinat.Fi));
            linkedPointSphere.BottomPoint = new LinkedPointSphere(new Point(bottomPointCoorinat.Teta + stepAngle, bottomPointCoorinat.Fi));
            linkedPointSphere.BottomLeftPoint = new LinkedPointSphere(new Point(bottomLeftPointCoordinat.Teta + stepAngle, bottomLeftPointCoordinat.Fi));

            linkedPointSphere.BottomRightPoint.TopPoint = linkedPointSphere.RightPoint;
            linkedPointSphere.BottomRightPoint.TopLeftPoint = linkedPointSphere;
            linkedPointSphere.BottomRightPoint.LeftPoint = linkedPointSphere.BottomPoint;

            linkedPointSphere.BottomPoint.TopRightPoint = linkedPointSphere.RightPoint;
            linkedPointSphere.BottomPoint.RightPoint = linkedPointSphere.BottomRightPoint;
            linkedPointSphere.BottomPoint.TopPoint = linkedPointSphere;
            linkedPointSphere.BottomPoint.TopLeftPoint = linkedPointSphere.LeftPoint;
            linkedPointSphere.BottomPoint.LeftPoint = linkedPointSphere.BottomLeftPoint;

            linkedPointSphere.BottomRightPoint.TopPoint = linkedPointSphere.LeftPoint;
            linkedPointSphere.BottomRightPoint.RightPoint = linkedPointSphere.BottomPoint;
            linkedPointSphere.BottomRightPoint.TopRightPoint = linkedPointSphere;

            return linkedPointSphere;
        }

        private LinkedPointSphere GetCircleFi(double stepAngle, LinkedPointSphere linkedPointSphere, LinkedPointSphere lastLinkedSphere)
        {
            linkedPointSphere = lastLinkedSphere.RightPoint;
            linkedPointSphere.TopPoint = lastLinkedSphere.TopRightPoint;
            linkedPointSphere.BottomPoint = lastLinkedSphere.BottomRightPoint;
            linkedPointSphere.TopLeftPoint = lastLinkedSphere.TopPoint;
            linkedPointSphere.LeftPoint = lastLinkedSphere;
            linkedPointSphere.BottomLeftPoint = lastLinkedSphere.BottomPoint;
            var topRightPoint = lastLinkedSphere.TopRightPoint.Coordinat;
            var rightPoint = lastLinkedSphere.RightPoint.Coordinat;
            var bottomRightPoint = lastLinkedSphere.BottomRightPoint.Coordinat;
            var rightPointTeta = rightPoint.Teta;
            if(rightPoint.Fi + stepAngle >= 180)
            {
                rightPointTeta = 360 - rightPointTeta;
            }

            linkedPointSphere.TopRightPoint = new LinkedPointSphere(new Point(rightPointTeta, topRightPoint.Fi + stepAngle));
            linkedPointSphere.RightPoint = new LinkedPointSphere(new Point(rightPointTeta, rightPoint.Fi + stepAngle));
            linkedPointSphere.BottomRightPoint = new LinkedPointSphere(new Point(rightPointTeta, bottomRightPoint.Fi + stepAngle));

            linkedPointSphere.TopRightPoint.LeftPoint = lastLinkedSphere.TopPoint;
            linkedPointSphere.TopRightPoint.BottomLeftPoint = lastLinkedSphere;
            linkedPointSphere.TopRightPoint.BottomPoint = linkedPointSphere.RightPoint;

            linkedPointSphere.BottomRightPoint.TopPoint = linkedPointSphere.RightPoint;
            linkedPointSphere.BottomRightPoint.TopLeftPoint = linkedPointSphere;
            linkedPointSphere.BottomRightPoint.LeftPoint = linkedPointSphere.BottomRightPoint;

            linkedPointSphere.RightPoint.TopPoint = linkedPointSphere.TopRightPoint;
            linkedPointSphere.RightPoint.BottomPoint = linkedPointSphere.BottomRightPoint;
            linkedPointSphere.RightPoint.TopLeftPoint = linkedPointSphere.TopPoint;
            linkedPointSphere.RightPoint.LeftPoint = linkedPointSphere;
            linkedPointSphere.RightPoint.BottomLeftPoint = linkedPointSphere.BottomPoint;

            return linkedPointSphere;
        }
    }
}
