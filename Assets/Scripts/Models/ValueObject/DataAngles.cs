﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models.ValueObject
{
    public class DataAngles
    {

        public double Radius { get; set; }

        public double Fi { get; set; }

        public double Teta { get; set; }

        public string NameModel { get; set; }

        public DataAngles(double fi, double teta, string name, double radius)
        {
            Radius = radius;
            Teta = teta;
            NameModel = name;
            Radius = radius;
        }

    }
}
