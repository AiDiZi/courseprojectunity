﻿using System.Collections.Generic;

namespace Assets.Scripts.Models
{
    public class CircleBufferPoint
    {
        public CircleBufferPoint LeftPoint { get; set; }
        
        public CircleBufferPoint RightPoint { get; set; }

        public Point Coordinat { get; }

        public List<Point> VerticalPassageBottom( double fi, double stepAngle)
        {
            var listTraversal = new List<Point>();

            if (fi > 180)
            {
                for (double i = 360; i > 180; i -= stepAngle)
                {
                    listTraversal.Add(new Point(i, fi));
                }
            }
            else
            {
                for (double i = 0; i <= 180; i+= stepAngle)
                {
                    listTraversal.Add(new Point(i, fi));
                }
            }

            return listTraversal;
        }


        public CircleBufferPoint(Point coordinat)
        {
            Coordinat = coordinat;
        }

        public List<CircleBufferPoint> HorizontalTraversal(CircleBufferPoint nowCircleBufferPoint, List<CircleBufferPoint> points)
        {
            if (this.Equals(nowCircleBufferPoint))
            {
                points.Add(nowCircleBufferPoint);
                return points;
            }
            else
            {
                points.Add(nowCircleBufferPoint);
                return HorizontalTraversal(nowCircleBufferPoint.RightPoint, points);
            }
        }

        public static CircleBufferPoint GetCircleBufferByFi(double stepAngle)
        {
            var startPoint = new CircleBufferPoint(new Point(90, 0));
            startPoint.RightPoint = new CircleBufferPoint(new Point(90, stepAngle));

            for(var i = stepAngle; i < 360; i+= stepAngle)
            {
                startPoint = GetNextPoint(new CircleBufferPoint(new Point(0, 0)), startPoint, stepAngle);
            }

            var startPointFi = RoollbackToStartFi(startPoint, 0);

            startPoint.RightPoint = startPointFi;

            startPointFi.LeftPoint = startPoint;

            startPoint = startPoint.RightPoint;

            return startPoint;
        }

        private static CircleBufferPoint GetNextPoint(CircleBufferPoint circleBufferPoint, CircleBufferPoint lastCirclePoint, double stepAngle)
        {
            circleBufferPoint = lastCirclePoint.RightPoint;
            circleBufferPoint.LeftPoint = lastCirclePoint;

            var coordinatRight = lastCirclePoint.RightPoint.Coordinat;

            var nowTeta = coordinatRight.Teta;
          

            circleBufferPoint.RightPoint = new CircleBufferPoint(new Point(nowTeta, coordinatRight.Fi + stepAngle));

            circleBufferPoint.RightPoint.LeftPoint = circleBufferPoint;

            return circleBufferPoint;
        }

        private static CircleBufferPoint RoollbackToStartFi(CircleBufferPoint linkedPointSphere, double angle)
        {
            if (angle == linkedPointSphere.Coordinat.Fi)
                return linkedPointSphere;
            else
                return RoollbackToStartFi(linkedPointSphere.LeftPoint, angle);
        }

        public bool Equals(CircleBufferPoint circleBufferPoint)
        {
            if (this.Coordinat.Fi == circleBufferPoint.Coordinat.Fi &&
               this.Coordinat.Teta == circleBufferPoint.Coordinat.Teta)
                return true;
            else
                return false;
        }

    }
}
