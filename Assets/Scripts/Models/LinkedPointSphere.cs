﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Models
{
    public class LinkedPointSphere
    {
        public LinkedPointSphere TopPoint { get; set; }

        public LinkedPointSphere TopRightPoint { get; set; }

        public LinkedPointSphere TopLeftPoint { get; set; }

        public LinkedPointSphere RightPoint { get; set; }

        public LinkedPointSphere LeftPoint { get; set; }

        public LinkedPointSphere BottomPoint { get; set; }

        public LinkedPointSphere BottomRightPoint { get; set; }

        public LinkedPointSphere BottomLeftPoint { get; set; }

        public Point Coordinat { get; }

        public LinkedPointSphere(Point coordinat)
        {
            Coordinat = coordinat;
        }

        public bool Equals(LinkedPointSphere linkedPoint)
        {
            if (Coordinat.Equals(linkedPoint.Coordinat))
                return true;
            return false;
        }

        public LinkedPointSphere GetLinkedPointSphereByAngles(double teta, double fi)
        {
            return GetPointByAnglesRealiztion(this, teta, fi);
        }

        private LinkedPointSphere GetPointByAnglesRealiztion(LinkedPointSphere linkedPoint,double teta, double fi)
        {
            if (linkedPoint.Coordinat.Fi == fi)
            {
                if (linkedPoint.Coordinat.Teta == teta)
                {
                    return linkedPoint;
                }
                else
                {
                    return GetPointByAnglesRealiztion(linkedPoint.TopPoint, teta, fi);
                }
            }
            else
                return GetPointByAnglesRealiztion(linkedPoint.RightPoint, teta, fi);
        }

        public List<LinkedPointSphere> MainDiagonalPassage()
        {
            var listPassage = Passage(new List<LinkedPointSphere>(), TopLeftPoint, (LinkedPointSphere point) => { return point.TopLeftPoint; });
            return listPassage;
        }

        public List<LinkedPointSphere> SideDiagonalPassage()
        {
            var listPassage = Passage(new List<LinkedPointSphere>(), TopRightPoint, (LinkedPointSphere point) => { return point.TopRightPoint; });
            return listPassage;
        }

        public List<LinkedPointSphere> VerticalPassage()
        {
            var listPassage = Passage(new List<LinkedPointSphere>(), TopPoint, (LinkedPointSphere point) => { return point.TopPoint; });
            return listPassage;
        }

        public List<LinkedPointSphere> HorizontalPassage()
        {
            var listPassage = Passage(new List<LinkedPointSphere>(), RightPoint, (LinkedPointSphere point) => { return point.RightPoint; });
            return listPassage;
        }

        public List<LinkedPointSphere> Passage(List<LinkedPointSphere> pointsList, LinkedPointSphere nowPoint, Func<LinkedPointSphere, LinkedPointSphere> getNextPoint)
        {
            if (this.Equals(nowPoint))
            {
                return pointsList;
            }
            else
            {
                pointsList.Add(nowPoint);
                return Passage(pointsList,getNextPoint.Invoke(nowPoint), getNextPoint);
            }
        }

        //public static LinkedPointSphere GetLinkedPoint(double startFi, double startTeta, double radius, double stepAngle )
        //{
        //    var startPoint = new LinkedPointSphere(new Point(startTeta, startFi, radius));
        //    startPoint.RightPoint = new LinkedPointSphere(new Point(startTeta, startFi + stepAngle, radius));
        //    startPoint.TopPoint = new LinkedPointSphere(new Point(startTeta - stepAngle, startFi, radius));
        //    startPoint.TopRightPoint = new LinkedPointSphere(new Point(startTeta - stepAngle, startFi + stepAngle, radius));
        //    startPoint.TopLeftPoint = new LinkedPointSphere(new Point(startTeta - stepAngle, startFi - stepAngle, radius));
        //    startPoint.LeftPoint = new LinkedPointSphere(new Point(startTeta, startFi - stepAngle, radius));
        //    startPoint.BottomLeftPoint = new LinkedPointSphere(new Point(startTeta + stepAngle, startFi - stepAngle, radius));
        //    startPoint.BottomPoint = new LinkedPointSphere(new Point(startTeta + stepAngle, startFi, radius));
        //    startPoint.BottomRightPoint = new LinkedPointSphere(new Point(startTeta - stepAngle, startFi + stepAngle, radius));
        //    return startPoint;
        //}

      

    }
}
