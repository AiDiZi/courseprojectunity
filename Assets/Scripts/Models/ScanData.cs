﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public class ScanData
    {

        public string NameModel { get; set; }

        public double Radius { get; set; }

        public List<Point> points;


        public ScanData(string nameModel, double radius, List<Point> points)
        {
            NameModel = nameModel;
            Radius = radius;
            this.points = points;
        }


    }
}
