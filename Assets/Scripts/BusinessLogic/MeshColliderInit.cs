﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
namespace Assets.Scripts.BusinessLogic
{
    public class MeshColliderInit
    {
        public void InitMeshCollider(GameObject gameObject, string tag = "")
        {
            gameObject.AddComponent<MeshCollider>();
            gameObject.tag = tag;
            {
                for(int i = 0; i < gameObject.transform.childCount; i++)
                {
                    InitMeshCollider(gameObject.transform.GetChild(i).gameObject, tag);
                }
            }

        }
    }
}
