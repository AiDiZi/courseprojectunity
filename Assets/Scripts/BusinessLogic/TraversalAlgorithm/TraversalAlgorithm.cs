﻿using Assets.Scripts.Helpers;
using Assets.Scripts.Models;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.BusinessLogic.TraversalAlgorithm
{
    public class TraversalAlgorithm
    {
        protected int _nowIndexPoint;

        protected List<Vector3> _traversalPoints;

        public TraversalAlgorithm(List<Vector3> treaversalPoints)
        {
            _nowIndexPoint = 0;
            _traversalPoints = treaversalPoints;
        }
        public void Rotate(int speed, Action<Vector3, float> drawAction)
        {
            int nowIndex = _nowIndexPoint;
            for (var i = nowIndex; i < nowIndex+ speed; i++)
            {
                if (_nowIndexPoint >= _traversalPoints.Count)
                {
                    throw new Exception();
                }
                drawAction.Invoke(_traversalPoints[i], 0.05f);
                _nowIndexPoint++;
            }
        }
    }
}
