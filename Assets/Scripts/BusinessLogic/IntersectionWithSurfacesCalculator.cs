﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.BusinessLogic
{
    public class IntersectionWithSurfacesCalculator
    {

        public Vector3 GetIntersection(GameObject gameObject, Vector3 origin, Vector3 point, float maxDistance)
        {
            Vector3 pointIntersection = new Vector3();
            var namesObject = GetNamesObject(gameObject, new List<string>());
            var rayCastHits = Physics.RaycastAll(point, gameObject.transform.position - point, maxDistance);
            for (int i = 0; i < rayCastHits.Length; i++)
            {
                if (rayCastHits[i].transform != null)
                {
                    if (namesObject.Any(item => item.Equals(rayCastHits[i].transform.gameObject.name)))
                    {
                        pointIntersection = rayCastHits[i].point;
                    }
                }
            }
            return pointIntersection;
        }

        private List<string> GetNamesObject(GameObject gameObject, List<string> names)
        {
            var name = gameObject.name;
            names.Add(name);
            for(int i=0;i < gameObject.transform.childCount; i++)
            {
                names = GetNamesObject(gameObject.transform.GetChild(i).gameObject, names);
            }
            return names;
        }
    }
}
