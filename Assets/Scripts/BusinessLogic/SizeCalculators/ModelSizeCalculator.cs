﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.BusinessLogic.SizeCalculators
{
    public class ModelSizeCalculator : ISizeCalculator
    {

        private Vector3 GetExtremeVector(GameObject gameObject, Vector3 vector)
        {
            var meshRoot = gameObject.GetComponent<MeshFilter>();
            if(meshRoot != null)
            {
                var vectorSize = CalcSizeModel(gameObject);
                var maxSizeX = GetMaxValue(vectorSize.x, vector.x);
                var maxSizeY = GetMaxValue(vectorSize.y, vector.y);
                var maxSizeZ = GetMaxValue(vectorSize.z, vector.z);
                vector = new Vector3(maxSizeX, maxSizeY, maxSizeZ);
            }
            else
            {
                for(int i=0; i< gameObject.transform.childCount; i++)
                {
                    vector = GetExtremeVector(gameObject.transform.GetChild(i).gameObject, vector);
                }
            }
            return vector;
        }

        public GameObject GetChildWithMaxSize(GameObject gameObject, double maxSize, GameObject maxObj)
        {
            var nowMeshFilter = maxObj.GetComponent<MeshFilter>();
            if (nowMeshFilter != null)
            {
                var nowSizeVector = nowMeshFilter.mesh.bounds.size;
                var nowSize = Math.Sqrt(Math.Pow(nowSizeVector.x, 2) + Math.Pow(nowSizeVector.y, 2) + Math.Pow(nowSizeVector.z, 2));
                if (nowSize > maxSize)
                {
                    maxSize = nowSize;
                    maxObj = gameObject;
                }
            }
            else
            {
                for (int i = 0; i < gameObject.transform.childCount; i++)
                {
                    maxObj = GetChildWithMaxSize(gameObject.transform.GetChild(i).gameObject, maxSize, maxObj);
                }
            }
            return maxObj;
        }

        public double GetRadiusVectorModel(GameObject gameObject)
        {
            var extremVector = GetExtremeVector(gameObject, new Vector3(0,0,0));

            return CalcSizeFromVector3d(extremVector, gameObject);
        }

        public GameObject GetObjectWithMesh(GameObject gameObject)
        {
            var meshFilter = gameObject.GetComponent<MeshFilter>();
            if (meshFilter == null)
            {
                for (int i = 0; i < gameObject.transform.childCount; i++) 
                {
                    gameObject = GetObjectWithMesh(gameObject.transform.GetChild(i).gameObject);
                }
            }
            return gameObject;
        }

        private float GetMaxValue(float first, float second)
        {
            if (Math.Abs(first) > Math.Abs(second))
                return first;
            else
                return second;
        }

        private Vector3 CalcSizeModel(GameObject gameObject)
        {
            var nowMesh = gameObject.GetComponent<MeshFilter>().mesh;
            var size = nowMesh.bounds.size;
            return new Vector3(size.x - gameObject.transform.position.x,
                               size.y - gameObject.transform.position.y, 
                               size.z- gameObject.transform.position.z);
        }

        private double CalcSizeFromVector3d(Vector3 vector3, GameObject gameObject)
        {
            return Math.Sqrt(Math.Pow(vector3.x + gameObject.transform.position.x,  2) + 
                             Math.Pow(vector3.y,  2) + 
                             Math.Pow(vector3.z, 2));
        }
    }
}
