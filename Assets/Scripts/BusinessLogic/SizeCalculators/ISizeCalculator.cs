﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.BusinessLogic.SizeCalculators
{
    public interface ISizeCalculator
    {
        double GetRadiusVectorModel(GameObject gameObject);
    }
}
