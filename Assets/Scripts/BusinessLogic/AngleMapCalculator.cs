﻿using Assets.Scripts.Models;
using System.Collections.Generic;

namespace Assets.Scripts.BusinessLogic
{
    public class AngleMapCalculator : IAngleCalculator
    {
        public List<Point> GetPointsTraversal(double stepAngle)
        {
            var traversalPoints = new List<Point>();

            var circleBufferPoint = CircleBufferPoint.GetCircleBufferByFi(stepAngle);

            var horizontalList = circleBufferPoint.HorizontalTraversal(circleBufferPoint.RightPoint, new List<CircleBufferPoint>());

            for(double i = 0; i < 360; i += stepAngle)
            {
                int j = 0;
                foreach(var nowPoint in horizontalList)
                {
                    var verticalPoints = GetVerticalPointsTraversal(stepAngle, nowPoint.Coordinat.Fi);

                    if (j % 2 == 0)
                        verticalPoints.Reverse();

                    traversalPoints.AddRange(verticalPoints);
                    j++;
                }
                circleBufferPoint = circleBufferPoint.RightPoint;
                horizontalList = circleBufferPoint.HorizontalTraversal(circleBufferPoint.RightPoint, new List<CircleBufferPoint>());
            }

            return traversalPoints;
        }

        private List<Point> GetVerticalPointsTraversal(double stepAngel , double nowFi)
        {
            var pointsTraversal = new List<Point>();

            for (double teta = 0; teta < 180; teta += stepAngel)
            {
                pointsTraversal.Add(new Point(teta, nowFi));
            }

            return pointsTraversal;
        }
    }
}
