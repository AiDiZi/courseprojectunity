﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;


public class Transformation
{

    public Vector3 GetCartesianCoordinat(double teta, double fi, double radius, float originX, float originY, float originZ)
    {
        var x = GetXUnitCoordinat(teta, fi, radius, originX);
        var y = GetYUnitCoordinat(teta, fi, radius, originY);
        var z = GetZUnitCoordinate(teta, fi, radius, originZ);

        var vector = new Vector3((float)x, (float)y, (float)z);

        return vector;
    }

    private double GetCoordinat(double teta, double fi, double radiusVectorLenght, Func<double, double, double, double> func)
    {
        var tetaRadian = teta * Math.PI / 180;
        var fiRadian = fi * Math.PI / 180;
        return func.Invoke(tetaRadian, fiRadian, radiusVectorLenght);
    }

    public double GetXUnitCoordinat(double teta, double fi, double radiusVectorLength, double startX)
    {
        Func<double, double, double, double> nowFunc =
                                                 (double tetaRadian, double fiRadian, double radiusVector) => { return Math.Sin(tetaRadian) * Math.Cos(fiRadian) * radiusVector + startX; };
        return GetCoordinat(teta, fi, radiusVectorLength, nowFunc);
    }

    public double GetYUnitCoordinat(double teta, double fi, double radiusVectorLength, double startY)
    {
        Func<double, double, double, double> nowFunc =
                                               (double tetaRadian, double fiRadian, double radiusVector) => { return Math.Cos(tetaRadian) * radiusVector + startY; };
        return GetCoordinat(teta, fi, radiusVectorLength, nowFunc);
        
     
    }

    public double GetZUnitCoordinate(double teta, double fi, double radiusVectorLength, double startZ)
    {

        Func<double, double, double, double> nowFunc =
                                                  (double tetaRadian, double fiRadian, double radiusVector) => { return Math.Sin(tetaRadian) * Math.Sin(fiRadian) * radiusVector + startZ; };
        return GetCoordinat(teta, fi, radiusVectorLength, nowFunc);
    }

}
