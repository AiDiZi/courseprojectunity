﻿using Assets.Scripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.BusinessLogic
{
    public class AngleCalculator
    {
        private List<Point> VerticalPassage(double teta, double fi, double stepAngle)
        {
            var listTraversal = new List<Point>();
            for (double i = teta; i < 360 + teta; i += stepAngle)
            {
                var nowTeta = i;
                if (nowTeta >= 360)
                    nowTeta = nowTeta - 360;
                listTraversal.Add(new Point(nowTeta, fi));
            }
            return listTraversal;
        }

        public List<Point> GetPointsTraversal(double stepAngle)
        {
            var pointsTraversal = new List<Point>();
            for (double i = 0; i <= 180; i += stepAngle)
            {
                pointsTraversal.Add(new Point(90, i));

                var verticalPoints = VerticalPassage(90, i, stepAngle);
                pointsTraversal.AddRange(verticalPoints);

            }
            return pointsTraversal;
        }

    }
}
