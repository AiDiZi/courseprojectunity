﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.BusinessLogic
{
    public class VectorsWorker
    {
        public double GetLenghtVector(Vector3 origin, Vector3 vector)
        {
            var radiusVector = vector - origin;
            return Math.Sqrt(Math.Pow(radiusVector.x, 2) + Math.Pow(radiusVector.y, 2) + Math.Pow(radiusVector.z, 2));
        }
        public double CalcAngleDeg(Vector3 firstVector, Vector3 secondVector)
        {
            var numerator = ScalarProduct(firstVector, secondVector);
            var denominator = CalcLenghtVector(firstVector) * CalcLenghtVector(secondVector);

            return Math.Acos(numerator / denominator) * 180 / Math.PI;
        }

        private double ScalarProduct(Vector3 firstVector, Vector3 secondVector)
        {
            return firstVector.x* secondVector.x + firstVector.y * secondVector.y + firstVector.z * secondVector.z;
        }

        private double CalcLenghtVector(Vector3 vector)
        {
            return Math.Sqrt(Math.Pow(vector.x, 2) + Math.Pow(vector.y, 2) + Math.Pow(vector.z, 2));
        }

        public bool IsCoDirected(Vector3 firstVector, Vector3 secondVector)
        {
            var scalarProduct = ScalarProduct(firstVector, secondVector);
            return (scalarProduct > 0);
        }

    }
}
