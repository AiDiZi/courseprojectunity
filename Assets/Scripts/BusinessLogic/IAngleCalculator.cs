﻿using Assets.Scripts.Models;
using System.Collections.Generic;

namespace Assets.Scripts.BusinessLogic
{
    public interface IAngleCalculator 
    {
        List<Point> GetPointsTraversal(double stepAngle);

    }
}
