﻿using Assets.Scripts.BusinessLogic;
using Assets.Scripts.Helpers;
using Assets.Scripts.Helpers.FileProvider;
using Assets.Scripts.Models;
using Assets.Scripts.Models.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    //uiAreas
    public GameObject settingsMenu;

    public GameObject mainMenu;

    public UnityEngine.UI.Dropdown formatDropDown;

    public UnityEngine.UI.InputField stepAngleArea;

    public UnityEngine.UI.InputField stepMinuteArea;

    public UnityEngine.UI.InputField stepSecondArea;

    public UnityEngine.UI.ScrollRect models;

    public UnityEngine.UI.ScrollRect allModels;

    public UnityEngine.UI.ScrollRect mapModels;

    public UnityEngine.UI.ScrollRect allMapModels;

    public UnityEngine.UI.Text pathToFile;

    public UnityEngine.UI.Button buttonForAddingModels;

    public UnityEngine.UI.Text outFilePath;

    //end uiAreas

    private readonly string settingsNameFile = "settings.txt";

    //parametrs

    private int _stepAngle = 6;

    private int _stepAngleMinute = 0;

    private int _stepAgleSecond = 0;

    private string _outFile = "out.txt";

    private List<string> _pathsForScan = new List<string>() { "3dModels\\car2" };

    private List<string> _mapsForScan = new List<string> { "MapModels\\street" };

    private OutFormatFile _outFormatFile = OutFormatFile.TextFile;


    //end parametrs

    //dependencies
    private TextProvider _textFileProvider = new TextProvider();

    private FilePathParser filePathParser = new FilePathParser();

    private FileFormatValidator fileFormatValidator = new FileFormatValidator();

    private Predicate<string> validatorOutFileExtension = new FileFormatValidator().IsTxtEternalFile;

    private ToDegConvertor _degConvertor = new ToDegConvertor();

    private void Start()
    {
        InitParametrs();
    }

    private void InitParametrs()
    {
        PopUp popUp = gameObject.GetComponent<PopUp>();
        if (_textFileProvider.IsExistFile("settings.txt"))
        {
            ReadOutFile();
        }
        else
        {
            popUp.SetText("Не удается найти файл settings.txt , поэтому выходным файлом будет " + _outFile, (object obj) => { });
            _textFileProvider.WriteToFile("settings.txt", _outFile);
        }
        if (GlobalSettings.ModelsForScan == null)
        {
            GlobalSettings.SetAllProperty(_stepAngle,_stepAngleMinute, _stepAgleSecond ,_outFormatFile, _pathsForScan, _mapsForScan, _outFile);
        }
        else
        {
            _outFormatFile = GlobalSettings.OutFormatFile;
            _pathsForScan = GlobalSettings.ModelsForScan;
            _mapsForScan = GlobalSettings.MapsForScan;
            _stepAngle = GlobalSettings.StepAngle;
            _stepAngleMinute = GlobalSettings.StepAngleMinute;
            _stepAgleSecond = GlobalSettings.StepAngleSecond;
        }
       

    }

    public void ReadOutFileHandler()
    {
        ReadOutFile();
        outFilePath.text = _outFile;
    }

    private void ReadOutFile()
    {
        PopUp popUp = gameObject.GetComponent<PopUp>();
        var outFile = _textFileProvider.ReadFromFile("settings.txt").Trim('\n', '\r').Trim();
        if (!_textFileProvider.IsExistFile(outFile))
        {
            _textFileProvider.CreateFile(outFile);

        }
        
        _outFile = outFile;
        if (filePathParser.GetExtensionFile(outFile) == "csv")
            _outFormatFile = OutFormatFile.ExelFile;
        else
            _outFormatFile = OutFormatFile.TextFile;
    }


    private void InitParametrsOnUI()
    {
        stepAngleArea.text = _stepAngle.ToString();
        formatDropDown.value = (int)_outFormatFile;
        stepMinuteArea.text = _stepAngleMinute.ToString();
        stepSecondArea.text = _stepAgleSecond.ToString();
        outFilePath.text = _outFile;
        var allPath3d = _textFileProvider.GetAllFilesInDirectory("Assets\\Resources\\3dModels");
        var allPathMaps = _textFileProvider.GetAllFilesInDirectory("Assets\\Resources\\MapModels");

        for (int i = 0; i < allPath3d.Length; i++)
        {
            if(fileFormatValidator.IsValidModel(allPath3d[i]))
                AddModelsPathsToUIAllList(filePathParser.GetNameFile(allPath3d[i]), allModels, _pathsForScan, models);
        }

        for (int i = 0; i < allPathMaps.Length; i++)
        {
            if (fileFormatValidator.IsValidModel(allPathMaps[i]))
                AddModelsPathsToUIAllList(filePathParser.GetNameFile(allPathMaps[i]), allMapModels, _mapsForScan, mapModels);
        }

        foreach (var path in _pathsForScan)
            AddModelsPathsToWork(filePathParser.GetNameFile(path), models);

        foreach(var path in _mapsForScan)
        {
            AddModelsPathsToWork(filePathParser.GetNameFile(path), mapModels);
        }
    }

    
    public void SaveParametrs()
    {
        var nowStepAngle = stepAngleArea.text;
        var nowOutFormat = formatDropDown.value;
        var nowStepAngleMinute = stepMinuteArea.text;
        var nowStepAngleSecond = stepSecondArea.text;

        var paths3DForScan = new List<string>();
        foreach(var path in _pathsForScan)
        {
            if (!path.Contains("3dModels"))
                paths3DForScan.Add("3dModels\\" + filePathParser.GetNameFileWithoutExtension(path));
            else
                paths3DForScan.Add(path);
        }

        var mapsForScan = new List<string>();
        foreach(var path in _mapsForScan)
        {
            if (!path.Contains("MapModels"))
                mapsForScan.Add("MapModels\\" + filePathParser.GetNameFileWithoutExtension(path));
            else
                mapsForScan.Add(path);
        }

        var invalidStr = GetInvalidTextUI();

        PopUp popUp = gameObject.GetComponent<PopUp>();

        if (invalidStr == string.Empty)
        {
            GlobalSettings.SetAllProperty(int.Parse(nowStepAngle), int.Parse(nowStepAngleMinute), int.Parse(nowStepAngleSecond),(OutFormatFile)nowOutFormat, paths3DForScan, mapsForScan, _outFile);
            popUp.SetText("Параметры успешно сохранены", (object obj) => { });
        }
        else
        {
            popUp.SetText(invalidStr, (object obj) => { });

        }
    }

    private string GetInvalidTextPropertes()
    {
        return GetInvalidText(_stepAngle.ToString(), _stepAngleMinute.ToString(), _stepAgleSecond.ToString());
    }
    
    private string GetInvalidTextUI()
    {
        var nowStepAngle = stepAngleArea.text;
        var nowStepAngleMinute = stepMinuteArea.text;
        var nowStepAngleSecond = stepSecondArea.text;
        return GetInvalidText(nowStepAngle, nowStepAngleMinute, nowStepAngleSecond);
    }

    private string GetInvalidText( string nowStepAngle, string nowStepAngleMinute, string nowStepAngleSecond)
    {
        var outStr = string.Empty;
        int stepAngleNum;
        if (!(int.TryParse(nowStepAngle, out stepAngleNum) && int.TryParse(nowStepAngleMinute, out stepAngleNum) && int.TryParse(nowStepAngleSecond, out stepAngleNum)))
        {
            outStr = "Один из параметров шага обхода имел неверный формат";
            

        }
        else
        {
            if (_degConvertor.GetDegFromNum(int.Parse(nowStepAngle), int.Parse(nowStepAngleMinute), int.Parse(nowStepAngleSecond)) >= 45)
            {
                outStr = "Значение шага обхода не может превышать 45 градусов";
            }
        }
        if (_pathsForScan.Count < 1 && _mapsForScan.Count < 1)
        {
            outStr = "Выберите хотя бы один объект для сканирования";
        }
        return outStr;
    }

    public void AddModelPath()
    {
        var pathsToModel = new string[3]; //StandaloneFileBrowser.OpenFilePanel("Выберите объект для сканирования", "", "*.*", true);
        for (int i = 0; i < pathsToModel.Length; i++)
        {
            var pathToModelName = filePathParser.GetNameFile(pathsToModel[i]);
            if (fileFormatValidator.IsValidModel(pathToModelName))
            {
                _pathsForScan.Add(pathToModelName);
                AddModelsPathsToWork(pathToModelName, models);
            }
        }
    }

    private void AddModelsPathsToUIAllList (string pathToModel, UnityEngine.UI.ScrollRect scroll, List<string> pathsList, UnityEngine.UI.ScrollRect scrollRect)
    {
        var lastChildrenPositionY = scroll.transform.position.y + 40f;
        if (scroll.content.childCount != 0)
            lastChildrenPositionY = scroll.content.GetChild(scroll.content.childCount - 1).position.y;
        var nowPathToModel = Instantiate(buttonForAddingModels, new Vector3(scroll.transform.position.x - 10, lastChildrenPositionY - 22, scroll.transform.position.z), Quaternion.identity);
        nowPathToModel.GetComponentInChildren<UnityEngine.UI.Text>().text = pathToModel;
        nowPathToModel.onClick.AddListener(() => { pathsList.Add(pathToModel); AddModelsPathsToWork(pathToModel, scrollRect); });
        nowPathToModel.transform.SetParent(scroll.content.transform);
    }

    private void AddModelsPathsToWork(string pathToModel, UnityEngine.UI.ScrollRect scroll)
    {
        var lastChildrenPositionY = scroll.transform.position.y + 40f;
        if (scroll.content.childCount != 0)
            lastChildrenPositionY = scroll.content.GetChild(scroll.content.childCount - 1).position.y;
        var nowPathToModel = Instantiate(pathToFile, new Vector3(scroll.transform.position.x - 10, lastChildrenPositionY - 22, scroll.transform.position.z), Quaternion.identity);
        nowPathToModel.transform.SetParent(scroll.content.transform);
        nowPathToModel.text = pathToModel;
    }

    public void Delete3DModelPath()
    {
        if (_pathsForScan.Count > 0)
        {
            _pathsForScan.Remove(_pathsForScan[_pathsForScan.Count - 1]);
            var lastComponent = models.content.GetChild(models.content.childCount - 1).gameObject;
            Destroy(lastComponent);
        }
    }

    public void DeleteMapPath()
    {
        if(_mapsForScan.Count > 0)
        {
            _mapsForScan.Remove(_mapsForScan[_mapsForScan.Count - 1]);
            var lastComponent = mapModels.content.GetChild(mapModels.content.childCount - 1).gameObject;
            Destroy(lastComponent);
        }

    }


    public void OpenSettingMenu()
    {
        settingsMenu.SetActive(true);
        mainMenu.SetActive(false);
        InitParametrs();
        InitParametrsOnUI();
    }

    private void DeleteFromUI(ScrollRect scroll)
    {
        for (int i = 0; i < scroll.content.childCount; i++)
        {
            var component = scroll.content.GetChild(i).gameObject;
            Destroy(component);
        }
    }

    public void BackToMainMenu()
    {
        settingsMenu.SetActive(false);
        mainMenu.SetActive(true);
        DeleteFromUI(models);
        DeleteFromUI(mapModels);
        DeleteFromUI(allModels);
        DeleteFromUI(allMapModels);
    }

    public void LoadScanScene()
    {
        var invalidParametrStr = GetInvalidTextPropertes();
        if (invalidParametrStr == string.Empty)
        {
            Application.LoadLevel("Overview");
        }
        else
        {
            PopUp popUp = gameObject.GetComponent<PopUp>();
            popUp.SetText(invalidParametrStr + ". Зайдите в раздел \"Настройки\"", (object obj) => { });
        }
    }

    public void Exit()
    {
        Application.Quit();
    }
    
}
