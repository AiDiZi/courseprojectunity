﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Behavior
{
    class Menu : MonoBehaviour
    {

        public GameObject MiddleMenu;

        private State _state;

        private void Start()
        {
            _state = GetComponent<State>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                MiddleMenu.SetActive(true);
                Time.timeScale = 0;
                _state.IsMovedCamera = false;
            }
        }

        public void MoveToMainMenu()
        {
            SetMenuOff();
            Cursor.visible = true;
            Application.LoadLevel("Menu");
        }

        public void SetMenuOff()
        {

            MiddleMenu.SetActive(false);
            Time.timeScale = 1;
            _state.IsMovedCamera = true;

        }
    }
}
