﻿using System;
using UnityEngine;

public class PopUp : MonoBehaviour
{

    public GameObject _popUpWindow;

    public UnityEngine.UI.Text _text;

    private Action<object> _closeAction;

    public void SetText(string text, Action<object> closeAction)
    {
        _popUpWindow.SetActive(true);
        _text.text = text;
        _closeAction = closeAction;
    }

    public void ClosePopUp()
    {
        _popUpWindow.SetActive(false);
        _closeAction.Invoke("Close");
    }
}
