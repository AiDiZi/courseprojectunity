﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class ScaningUI : MonoBehaviour
    {
        public GameObject _scanPoint; 
        public void InitPointInScene(Vector3 point, float scale)
        {
            _scanPoint.transform.localScale = new Vector3(scale, scale, scale);
            Instantiate(_scanPoint, point, Quaternion.identity);
        }
    }
}
