﻿using Assets.Scripts.Behavior;
using Assets.Scripts.BusinessLogic;
using Assets.Scripts.BusinessLogic.SizeCalculators;
using Assets.Scripts.Models;
using System.Collections.Generic;
using UnityEngine;

public class SelectObject : MonoBehaviour
{
    private string selectableTag = "ObjectToScan";

    public Material _selectedMaterial;

    private Material[] _lastMaterials;
    
    public GameObject _selection;

    private ScaningActionBehavior _scaningActionBehavior;

    private void Start()
    {
        _scaningActionBehavior = GetComponent<ScaningActionBehavior>();
    }

    void Update()
    {
        GetCroosObject();
        if (Input.GetKey(KeyCode.C) && _selection != null)
        {
            var scanDatas = _scaningActionBehavior.ScanMap(_selection);
            _scaningActionBehavior.WriteResults(scanDatas);
        }
    }

    private void GetCroosObject()
    {
        if (_selection != null)
        {
            var renderer = _selection.GetComponent<MeshRenderer>();
            renderer.materials = _lastMaterials;
            _selection = null;
        }
        RaycastHit raycastHit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out raycastHit))
        {
            var selection = raycastHit.transform;
            if (selection.gameObject.CompareTag(selectableTag))
            {
                var selectionObject = selection.gameObject;
                var selectionRenderer = selection.GetComponent<MeshRenderer>();
                if (selectionObject != null && selectionRenderer != null)
                {
                    _lastMaterials = selectionRenderer.materials;
                    var nowMaterials = new Material[_lastMaterials.Length];
                    for(int i = 0; i < nowMaterials.Length; i++)
                    {
                        nowMaterials[i] = _selectedMaterial;
                    }
                    selectionRenderer.materials = nowMaterials; 
                }
                _selection = selection.gameObject;
            }
        }
    }

}
