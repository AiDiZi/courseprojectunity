﻿using Assets.Scripts.Behavior;
using Assets.Scripts.BusinessLogic;
using Assets.Scripts.BusinessLogic.SizeCalculators;
using Assets.Scripts.BusinessLogic.TraversalAlgorithm;
using Assets.Scripts.Models;
using Assets.Scripts.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    private Transform _learningObject;

    public UnityEngine.UI.Slider slider;

    private int _nowIndexObject = 0;

    private List<GameObject> _gamesObjects;

    private int _speed = 2;

    private int _borderIndex;

    private ScanData _nowScanData;

    private List<string> _nameModels = new List<string>();

    private TraversalAlgorithm _traversalAlgorithm;

    private ScaningActionBehavior _scaningActionBehavior;

    private ModelSizeCalculator _sizeCalcualtor = new ModelSizeCalculator();

    private ScaningUI _scaningUI;

    private bool _isEndScan3d = false;

    void Start()
    {
        _scaningActionBehavior = GetComponent<ScaningActionBehavior>();
        _scaningUI = GetComponent<ScaningUI>();
        InitModelsOnScene();
        StayModelsOnSceneValid();
        ResetVariables();
        InitUiArea();   
    }

    private void ResetVariables()
    {
        var nowObj = GetObjectByIndex(_gamesObjects,_nowIndexObject);
        _learningObject = nowObj.transform;
        if (nowObj == null)
        {
            throw new Exception("Null Object");
        }
        else
        {
            var scanData = _scaningActionBehavior.Scan3DModel(nowObj);
            _nowScanData = scanData;
            var pointsIntersection = new List<Vector3>();
            foreach (var point in scanData.points)
                pointsIntersection.Add(point.PointIntersection);
            _traversalAlgorithm = new TraversalAlgorithm(pointsIntersection);
            if (_borderIndex <= _nowIndexObject)
            {
                _isEndScan3d = true;
            }
            transform.position = _learningObject.position;
        }
        
    }

    private GameObject GetObjectByIndex(List<GameObject> gamesObjects,int index)
    {
        GameObject nowObj = null;
        if (index < gamesObjects.Count)
        {
            nowObj = gamesObjects[index];
        }
        return nowObj;
    }

    private void InitUiArea()
    {
        slider.value = _speed/10f;
    }

    private double GetMaxSize(List<GameObject> gamesObjects)
    {
        var maxSize = 0.0;
        for (int i = 0; i < gamesObjects.Count; i++)
        {
            var nowSize = _sizeCalcualtor.GetRadiusVectorModel(gamesObjects[i]);
            if (nowSize > maxSize)
                maxSize = nowSize;
        }
        return maxSize;
    }

    private void StayModelsOnSceneValid()
    {
        var maxSize = GetMaxSize(_gamesObjects);
        _gamesObjects[0].transform.position = new Vector3(0, 0, 0);
        for(int i=1; i< _gamesObjects.Count; i++)
        {
            _gamesObjects[i].transform.position = new Vector3(2 * (float)maxSize * i, 0, 0);
        }
    }

    private void InitModelsOnScene()
    {
        var meshColliderInit = new MeshColliderInit();
        var gamesObjects = new List<GameObject>();
        var allModels = GlobalSettings.ModelsForScan.Union(GlobalSettings.MapsForScan).ToList();
        _borderIndex = GlobalSettings.ModelsForScan.Count;
        for (int i = 0; i < allModels.Count; i++)
        {
            var nowModel = Resources.Load<GameObject>(allModels[i]);
            if (nowModel != null)
            {
                var nowModelOnScene = Instantiate(nowModel).gameObject;
                var tag = string.Empty;
                if (_borderIndex <= i)
                    tag = "ObjectToScan";
                meshColliderInit.InitMeshCollider(nowModelOnScene, tag);
                gamesObjects.Add(nowModelOnScene);
                _nameModels.Add(allModels[i]);
            }
        }

        _gamesObjects = gamesObjects;
    }

    void FixedUpdate()
    {
        if (_learningObject != null && !_isEndScan3d)
        {
            Rotate();
        }
    }

    private void Rotate()
    {
        try
        {
            _traversalAlgorithm.Rotate(_speed, _scaningUI.InitPointInScene);
        }
        catch
        {
            _learningObject = null;
            _scaningActionBehavior.WriteResults(_nowScanData);
            _nowIndexObject++;
            GetNextObject();
        }

    }

    private void GetNextObject()
    {
        try
        {
            ResetVariables();
        }
        catch
        {
            _scaningActionBehavior.EndScan();
        }
    }

    public void ChangeValueSpeed()
    {
        if (slider.value == 0)
        {
            _speed = 0;
        }
        else
        {
            Time.timeScale = 1;
            int speed = (int)(slider.value * 10);
            if (speed == 0)
                speed = 1;
            _speed = speed;
        }
    }
}
