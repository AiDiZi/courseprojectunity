﻿using Assets.Scripts.Behavior;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeCameraState : MonoBehaviour
{

    public Camera _mainCamera;

    public Slider _slider;

    private State _state;

    public GameObject _hitMarker;

    private void Start()
    {
        _state = GetComponent<State>();
    }
    public void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (_state.IsMovedCamera)
            {
                _slider.gameObject.SetActive(true);
                _state.IsMovedCamera = false;
            }
            else
            {
                _slider.gameObject.SetActive(false);
                _state.IsMovedCamera = true; 
            }
        }
    
    }
}
