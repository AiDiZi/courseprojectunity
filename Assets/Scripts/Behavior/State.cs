﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Behavior
{
    public class State : MonoBehaviour
    {
        private bool isMovedCamera = false;

        public GameObject _hitMarker;

        public bool IsMovedCamera 
        { 
            get 
            {
                return isMovedCamera;
            }
            set
            {
                ChangeVisibleCursor(!value);
                isMovedCamera = value;
            }
        }

        void ChangeVisibleCursor(bool value)
        {
            Cursor.visible = value;
            _hitMarker.SetActive(!value);
        }
    }
}
