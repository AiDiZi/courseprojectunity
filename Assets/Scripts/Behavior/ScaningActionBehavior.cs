﻿using Assets.Scripts.BusinessLogic;
using Assets.Scripts.BusinessLogic.SizeCalculators;
using Assets.Scripts.Helpers;
using Assets.Scripts.Helpers.FileProvider;
using Assets.Scripts.Helpers.Writer;
using Assets.Scripts.Models;
using Assets.Scripts.UI;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Behavior
{
    public class ScaningActionBehavior : MonoBehaviour
    {
        private List<Point> _pointsSphere;

        private PopUp _popUp;

        private IFormatStr _formatContext;

        private TextProvider _textProvider;

        private State _state;

        private ScaningUI _scaningUI;

        private Transformation _transformation = new Transformation();

        private ModelSizeCalculator _sizeCalcualtor = new ModelSizeCalculator();

        private VectorsWorker _vectorsWorker = new VectorsWorker();

        private IntersectionWithSurfacesCalculator _intersectionWithSurfaces = new IntersectionWithSurfacesCalculator();

        private void Start()
        {
            _formatContext = FileContextDictionary.FileDictionaty[(int)GlobalSettings.OutFormatFile];
            _popUp = GetComponent<PopUp>();
            _textProvider = new TextProvider();
            _state = GetComponent<State>();
            _textProvider.CreateFile(GlobalSettings.OutFile);
            _scaningUI = GetComponent<ScaningUI>();

            var angleGenerator = new AngleCalculator();
            var stepAngle = new ToDegConvertor().GetDegFromNum(GlobalSettings.StepAngle, GlobalSettings.StepAngleMinute, GlobalSettings.StepAngleSecond);
            _pointsSphere = angleGenerator.GetPointsTraversal(stepAngle);
        }

        public void WriteResults(ScanData scanData)
        {
            var outStr = _formatContext.GetStringToWrite(scanData, GlobalSettings.OutFile, 3);
            _textProvider.AppendOnFile(GlobalSettings.OutFile, outStr);
            ShowPopUpText("Данные успешно записаны в файл ");
        }

        private ScanData ScanObject(GameObject gameObject, Func<Vector3, Vector3,bool> condition)
        {
            var points = new List<Point>();
            var radius = _sizeCalcualtor.GetRadiusVectorModel(gameObject);
            foreach (var point in _pointsSphere)
            {
                var vector = _transformation.GetCartesianCoordinat(point.Teta, point.Fi, radius
                                                                   , gameObject.transform.position.x,
                                                                   gameObject.transform.position.y,
                                                                   gameObject.transform.position.z);
                if (!condition.Invoke(transform.position - gameObject.transform.position, gameObject.transform.position - vector))
                {
                    var pointIntersection = _intersectionWithSurfaces.GetIntersection(gameObject.gameObject,
                                                                                      gameObject.transform.position,
                                                                                      vector,
                                                                                      (float)radius);
                    points.Add(new Point(point.Teta, point.Fi, _vectorsWorker.GetLenghtVector(gameObject.transform.position, pointIntersection), pointIntersection));
                }
            }
            return new ScanData(gameObject.name, 1, points);
        }

        public ScanData ScanMap(GameObject gameObject)
        {
            var scanData = ScanObject(gameObject, _vectorsWorker.IsCoDirected);
            var points = new List<Vector3>();
            scanData.points.ForEach(item => points.Add(item.PointIntersection));

            foreach(var point in points)
            {
                _scaningUI.InitPointInScene(point, 0.08f);
            }

            return scanData;
        }

        public ScanData Scan3DModel(GameObject gameObject)
        {
            return ScanObject(gameObject, (Vector3 first, Vector3 second) => { return false; });
        }

        public void ShowPopUpText(string text)
        {
            _state.IsMovedCamera = false;
            _popUp.SetText(text + GlobalSettings.OutFile, (object obj) => { _state.IsMovedCamera = true; });
        }

        public void EndScan()
        {
            _state.IsMovedCamera = false;
            _popUp.SetText("Данные успешно записаны в файл " + GlobalSettings.OutFile, (object obj) => { Application.LoadLevel("Menu"); });
        }

    }
}
