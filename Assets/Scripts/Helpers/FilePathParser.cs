﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Helpers
{
    public class FilePathParser
    {
        public string GetNameFile(string fullPath)
        {
            var nameFile = fullPath.Split('/','\\');
            return nameFile[nameFile.Length - 1];
        }

        public string GetExtensionFile(string path)
        {
            var name = GetNameFile(path);
            var nameStr = name.Split('.');
            return nameStr[nameStr.Length - 1];
        }

        public string GetNameFileWithoutExtension(string path)
        {
            var nameStr = path.Split('.');
            return nameStr[0];
        }

    }
}
