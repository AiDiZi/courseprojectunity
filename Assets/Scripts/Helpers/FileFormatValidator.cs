﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Helpers
{
    public class FileFormatValidator
    {

        private string GetExtensionFile(string nameFile)
        {
            var extensionFileArr = nameFile.Split('.');
            return extensionFileArr[extensionFileArr.Length - 1];
        }

        private bool IsHaveArrStr(string extensionFile, string[] formats)
        {
            if (formats.Any(item => item == extensionFile))
            {
                return true;
            }
            return false;
        }

        public bool IsExelExtensionFile(string nameFile)
        {
            var formats = new string[] { "csv" };
            var extensionFile = GetExtensionFile(nameFile);
            return IsHaveArrStr(extensionFile, formats);
        }

        public bool IsTxtEternalFile(string nameFile)
        {
            var formats = new string[] { "txt" };
            var extensionFile = GetExtensionFile(nameFile);
            return IsHaveArrStr(extensionFile, formats);
        }

        public bool IsValidModel(string nameFile)
        {
            var formats = new string[] {"obj", "blend" };
            var extensionFile = GetExtensionFile(nameFile);
            return IsHaveArrStr(extensionFile, formats);
        }

        public bool IsValidOutFile(string nameFile)
        {
            var extensionFile = GetExtensionFile(nameFile);
            var formats = new string[] { "txt", "csv" };
            return IsHaveArrStr(extensionFile, formats);
        }
    }
}
