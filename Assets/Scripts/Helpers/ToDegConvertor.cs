﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Helpers
{
    public class ToDegConvertor
    {

        public double GetDegFromNum(int deg,int minute, int second)
        {
            var nowMinute = 0;
            var outDeg = deg;
            var stepsSeconds = second / 60;
            var remainderSeconds = GetRemainderSeconds(second % 60);
            for (int i = 0; i < stepsSeconds; i++)
                nowMinute++;
            var stepsMinute = (minute + nowMinute) / 60;
            var remainderMinutes = GetRemainderMinutes(minute % 60);
            for (int i = 0; i < stepsMinute; i++)
                outDeg++;
            return outDeg + remainderMinutes + remainderSeconds;
        }

        private double GetRemainderMinutes(int value)
        {
            return (double)value / 60.0;
        }

        private double GetRemainderSeconds(int value)
        {
            return (double)value / 3600;
        }

    }
}
