﻿using Assets.Scripts.Models;
using Assets.Scripts.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Helpers
{
    public class SettingsParser
    {
        private readonly ToDegConvertor _degConvertor = new ToDegConvertor();

        private readonly FilePathParser filePathParser = new FilePathParser();

        public List<string> GetMapsForScan(string[] parametrsStr)
        {
            var mapsForScan = new List<string>();

            var index = 4;
            var nowStr = parametrsStr[3];
            while (nowStr != string.Empty)
            {
                index++;
                nowStr = parametrsStr[4 + index];
            }

            for(int i = index; i < parametrsStr.Length - 1; i++)
            {
                mapsForScan.Add(filePathParser.GetNameFileWithoutExtension(parametrsStr[i]));
            }

            return mapsForScan;
        }

        public List<string> GetModelsForScan(string[] parametrsStr)
        {
            var pathsForScan = new List<string>();

            int countModels = 0;
            string nowModel = parametrsStr[2];

            while (nowModel != string.Empty)
            {
                countModels++;
                nowModel = parametrsStr[countModels + 2];
            }

            for (int i = 2; i < countModels + 2; i++)
            {
                pathsForScan.Add(filePathParser.GetNameFileWithoutExtension(parametrsStr[i]));
            }

            return pathsForScan;
        }

        public OutFormatFile GetOutFormatFile(string[] parametrsStr) 
        {
            return (OutFormatFile)int.Parse(parametrsStr[0]);
        }

        
        public double ParseStepAngle(string[] parametrsStr)
        {
            var angles = parametrsStr[1].Split(' ');
            return _degConvertor.GetDegFromNum(int.Parse(angles[0]), int.Parse(angles[1]), int.Parse(angles[2]));
        }

        public Angle GetExtensionAngle(string[] parametrStr)
        {
            var angles = parametrStr[1].Split(' ');
            return new Angle(int.Parse(angles[0]), int.Parse(angles[1]), int.Parse(angles[2]));
        }
    }
}
