﻿using System.IO;
using System.Text;

namespace Assets.Scripts.Helpers.FileProvider
{
    public class TextProvider
    {

        public void CreateFile(string path)
        {
            File.Create(path);
        }
        public bool IsExistFile(string path)
        {
            return File.Exists(path);
        }

        public void AppendOnFile(string path, string text)
        {
            File.AppendAllText(path, text, Encoding.Unicode);
        }
        public void WriteToFile(string path, string text, FileMode fileMode = FileMode.Create)
        {
            var fs = new FileStream(path, fileMode);
            using (StreamWriter streamWriter = new StreamWriter(fs, Encoding.Unicode))
            {
                streamWriter.Write(text);
            }
        }

        public string ReadFromFile(string path)
        {
            string outStr = string.Empty;
            using(StreamReader streamReader = new StreamReader(path))
            {
                outStr = streamReader.ReadToEnd();
            }
            return outStr;
        }

        public string[] GetAllFilesInDirectory(string path)
        {
            return Directory.GetFiles(path);
        }

    }
}
