﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Assets.Scripts.Models;
using Assets.Scripts.Models.ValueObject;
using Assets.Scripts.Helpers.FileProvider;

namespace Assets.Scripts.Helpers.Writer
{
    public class CSVFormat : IFormatStr
    {
        private readonly TextProvider _textProvider = new TextProvider();

        private StringBuilder GetList(ScanData scanData, int accuracy)
        {
            var outStr = new StringBuilder("");
           
            foreach(var point in scanData.points)
            {
              
                var nowStr = $"{scanData.NameModel};{point.Fi};{point.Teta};{Math.Round(point.Radius,accuracy)}\n";
                outStr.Append(nowStr);
            }
            return outStr;
        }

        public string GetStringToWrite(ScanData scanData, string filePath, int accuracy)
        {
            var header = GetHeader();
            var str = GetList(scanData, accuracy) + "\n";

            return header + str;
        }

        private StringBuilder GetHeader()
        {
            return new StringBuilder("Имя объекта;Угол \u03C6;Угол \u03F4;Радиус\n");
        }

        public string GetStringToWrite(List<ScanData> scanDatas, string filePath, int accuracy)
        {
            var outList = GetHeader();
            foreach(var scanData in scanDatas)
            {
                var nowList = GetList(scanData, accuracy);
                nowList.Append("\n");
                outList.Append(nowList);
            }
            return outList.ToString();
        }
    }
}
