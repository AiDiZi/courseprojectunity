﻿using Assets.Scripts.Helpers.FileProvider;
using Assets.Scripts.Helpers.Writer;
using Assets.Scripts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assets.Scripts.BusinessLogic
{
    public class TxtFormat : IFormatStr
    {

        private readonly TextProvider _textProvider = new TextProvider();

      

        private string ConvertToOutStr(ScanData scanData,  int accuracy)
        {
            var outStr = new StringBuilder(scanData.NameModel + "\n");
            foreach(var point in scanData.points)
            {
                outStr.Append(scanData.NameModel + " " + point.Fi + " " + point.Teta + " " + Math.Round( point.Radius, accuracy) + "\n");
            }
            return outStr.ToString();
        }

        private StringBuilder GetHeader()
        {
            return new StringBuilder("Имя объекта  Угол \u03C6  Угол \u03F4  Радиус\n");
        }

        public string GetStringToWrite(ScanData scanData, string filePath, int accuracy)
        {
            var header = GetHeader();
            var str = ConvertToOutStr(scanData, accuracy) + "\n";

            return header + str;
        }

        public string GetStringToWrite(List<ScanData> scanDatas, string filePath, int accuracy)
        {
            var outStr = new StringBuilder("");
            foreach(var scanData in scanDatas)
            {
                outStr.Append(ConvertToOutStr(scanData, accuracy));
                outStr.Append("\n");
            }
            return outStr.ToString();
        }

    }
}
