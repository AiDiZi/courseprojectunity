﻿using Assets.Scripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Helpers.Writer
{
    public interface IFormatStr
    {
        string GetStringToWrite(List<ScanData> scanDatas, string filePath, int accuracy);

        string GetStringToWrite(ScanData scanData, string filePath, int accuracy);
    }
}
